# PokerAPI

This project was generated with Express JS.

## Install dependencies

Run `npm install` to install all project dependencies.

## Development server

Run `npm start` for a dev server. Send a `String` as this example: `AOJOKO10OQO,7C9C5C6C8C,KPJP10EQE9E,4E4C4P4O5E,2E2P2P3P3E,QCQPAEKCKE` to `http://localhost:3000/poker` to get the winner hand
