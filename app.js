var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors')

var pokerRouter = require('./routes/poker');

var app = express();

app.use(cors())

app.use('/', pokerRouter);

module.exports = app;
