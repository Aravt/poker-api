const express = require('express');
const router = express.Router();
const handsOrder = ["High Card", "Pair", "Double Pair", "Three of Kind", "Straight", "Flush", "Full House", "Four of Kind", "Straight Flush", "Royal Flush"];
const cardValue = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];

router.get('/poker', function (req, res, next) {
  let hands = req.query.hands.split(',');
  let handsKinds = [];
  let handsSuits = [];
  let highestsCards = [];

  separateSuits(hands);
  separateKinds(hands);
  sortCards(handsSuits, handsKinds);
  verifyEqualCards(hands, handsKinds);
  verifyFlush(hands);
  verifyStraight(hands);
  verifyHighestCard(hands, handsKinds);

  function verifyEqualCards(hands, handsKinds) {
    handsKinds.map((handKind, index) => {
      let newHand = [...new Set(handKind)];

      if (newHand.length == 5) {
        hands[index].name = "";
        hands[index].rank = 0;
      }

      if (newHand.length == 4) {
        hands[index].name = "Pair"
        hands[index].rank = 1;
      }

      if (newHand.length == 3) {
        let counter = countRepetition(handKind);

        if (counter[0] == 3 || counter[1] == 3 || counter[2] == 3) {
          hands[index].name = "Three of Kind";
          hands[index].rank = 3;
        } else {
          hands[index].name = "Double Pair";
          hands[index].rank = 2;
        }
      }

      if (newHand.length == 2) {
        let counter = countRepetition(handKind);

        if (counter[0] == 3 || counter[1] == 3) {
          hands[index].name = "Full House";
          hands[index].rank = 6;
        } else {
          hands[index].name = "Four of Kind";
          hands[index].rank = 7;
        }
      }
    });
  }

  function countRepetition(hand) {
    let current = null;
    let count = 0;

    let counter = {};

    for (let i = 0; i < hand.length; i++) {
      if (hand[i] != current) {
        if (count > 0) {
          counter[current] = count;
        }
        current = hand[i];
        count = 1;
      } else {
        count++;
      }
    }
    if (count > 0) {
      counter[current] = count;
    }

    let sortedCounter = Object.values(counter);
    sortedCounter.sort((a, b) => b - a);

    return sortedCounter;
  }

  function sortCards(suits, kinds) {
    let listToSort = [];

    for (let i = 0; i < suits.length; i++) {
      listToSort[i] = [];
      suits[i].map((suit, index) => {
        listToSort[i].push({
          suit: suit,
          kind: kinds[i][index]
        });
      })
    }

    listToSort.map(handCards => {
      handCards.sort((a, b) => {
        return cardValue.indexOf(b.kind) - cardValue.indexOf(a.kind);
      });
    });

    hands = listToSort;
  }

  function separateSuits(hands) {
    hands.map(hand => {
      handsSuits.push(hand.replace(/[^(C|O|E|P)]/g, "").split(""));
    });
  }

  function separateKinds(hands) {
    hands.map(hand => {
      var kinds = hand.split(/C|O|E|P/g);
      kinds.pop();

      handsKinds.push(kinds);
    });
  }

  function verifyFlush(hands) {
    hands.map(hand => {
      let suit = hand[0].suit;

      for (let card of hand) {
        if (card.suit != suit)
          return hand.isFlush = false;
      }

      return hand.isFlush = true;
    });
  }

  function verifyHighestCard(hands, kinds) {
    let highest = 0;

    highestsCards = kinds.map((kind, index) => {
      if (kind.indexOf("A") > -1) {
        const highestCard = {
          value: cardValue.indexOf("A"),
          player: index + 1
        };

        return hands[index].highestCard = highestCard;
      }

      if (kind.indexOf("K") > -1) {
        const highestCard = {
          value: cardValue.indexOf("K"),
          player: index + 1
        };

        return hands[index].highestCard = highestCard;
      }

      if (kind.indexOf("Q") > -1) {
        const highestCard = {
          value: cardValue.indexOf("Q"),
          player: index + 1
        };

        return hands[index].highestCard = highestCard;
      }

      if (kind.indexOf("J") > -1) {
        const highestCard = {
          value: cardValue.indexOf("J"),
          player: index + 1
        };

        return hands[index].highestCard = highestCard;
      }

      return kind.map((value, kIndex) => {
        if (value > highest) {
          highest = value;
        }

        if (kind.length == kIndex + 1) {
          const highestCard = {
            value: cardValue.indexOf(highest),
            player: index + 1
          };

          return hands[index].highestCard = highestCard;
        }
      })[kind.length - 1]
    });

    highestsCards.sort((a, b) => b.value - a.value);
  };

  function verifyStraight(hands) {
    hands.map(hand => {
      if (!hand[4]) {
        return hand.isStraight = false;
      }

      let r0 = cardValue.indexOf(hand[0].kind);
      let r4 = cardValue.indexOf(hand[4].kind);
      return hand.isStraight = (r0 - r4) == 4 && hand.rank == 0;
    });

  };

  function result(hands) {
    let winner = {
      player: 0,
      hand: "",
      highestCard: 0,
      rank: 0,
    };
    
    hands.map(hand => {
      if (hand.isStraight && hand.isFlush && hand.highestCard.value == 12) {
        hand.name = "Royal Flush";
        hand.rank = 9;
      } else if (hand.isStraight && hand.isFlush) {
        hand.name = "Straight Flush"
        hand.rank = 8;
      } else if (hand.isFlush) {
        hand.name = "Flush"
        hand.rank = 5;
      } else if (hand.isStraight) {
        hand.name = "Straight"
        hand.rank = 4;
      }

      if (hand.rank > winner.rank) {
        winner = {
          player: hand.highestCard.player,
          hand: hand.name,
          highestCard: hand.highestCard.value,
          rank: hand.rank,
        }
      } else if (hand.rank == winner.rank) {
        if (hand.highestCard.value > winner.highestCard) {
          winner = {
            player: hand.highestCard.player,
            hand: hand.name,
            highestCard: hand.highestCard.value,
            rank: hand.rank,
          }
        } else if (hand.highestCard.value == winner.highestCard) {
          winner = {};
        }
      }

      return winner;
    });

    res.send(winner)
  };

  result(hands);
});



module.exports = router;